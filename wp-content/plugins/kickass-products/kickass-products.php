<?php
/**
 * Plugin Name: KickAss Products
 * Description: Basic WordPress Plugin for managing products
 * Version:     1.0
 * Author:      Marko Petrovic
 * Author URI:  https://petrovic.design
 * License:     GPL2
 * License URI: https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain: kickassproducts
 */

// Prevent direct file access
if (!defined('ABSPATH')) {
    die('You can\'t access this file');
}

define('KICKASS_PLUGIN', __FILE__);
define('KICKASS_PLUGIN_DIR', untrailingslashit(dirname(KICKASS_PLUGIN)));

if (!function_exists('kickass_plugin_activate')) {
    function kickass_plugin_activate()
    {

        // Set the labels
        $labels = array(
            'name'                => __('Products'),
            'singular_name'       => __('Products'),
            'menu_name'           => __('Products'),
            'all_items'           => __('All Products'),
            'view_item'           => __('View Product'),
            'add_new_item'        => __('Add New Product'),
            'add_new'             => __('Add New Product'),
            'edit_item'           => __('Edit Product'),
            'featured_image'      => __('Image'),
            'set_featured_image'  => __('Set Image'),
            'update_item'         => __('Update Product'),
            'search_items'        => __('Search Product'),
            'not_found'           => __('Not Found'),
            'not_found_in_trash'  => __('Not found in Trash')
        );

        // register custom post type
        register_post_type(
            'products',
            array(
                'labels'             => $labels,
                'public'             => true,
                'publicly_queryable' => true,
                'show_ui'            => true,
                'show_in_menu'       => true,
                'rewrite'            => array('slug' => 'products'),
                'capability_type'    => 'post',
                'has_archive'        => true,
                'hierarchical'       => false,
                'menu_position'      => 5,
                'supports'           => array('title', 'thumbnail'),
                'register_meta_box_cb' => 'product_link_metabox'
            )
        );

        flush_rewrite_rules();
    }
}

if (file_exists(KICKASS_PLUGIN_DIR . '/includes/metaboxes.php')) {
    require_once KICKASS_PLUGIN_DIR . '/includes/metaboxes.php';
}

if (!function_exists('kickass_plugin_deactivate')) {
    function kickass_plugin_deactivate()
    {
        flush_rewrite_rules();

        // deregister custom post type
        if (post_type_exists('products')) {
            unregister_post_type('products');
        }
    }
}

add_action('init', 'kickass_plugin_activate');

function kickass_flush_rewrites()
{
    kickass_plugin_activate();
    flush_rewrite_rules();
}

// trigger on plugin activation
register_activation_hook(__FILE__, 'kickass_flush_rewrites');
// add_action('init', 'kickass_plugin_activate');

// trigger on plugin deactivation
register_deactivation_hook(__FILE__, 'kickass_plugin_deactivate');

if (file_exists(KICKASS_PLUGIN_DIR . '/includes/shortcodes.php')) {
    require_once KICKASS_PLUGIN_DIR . '/includes/shortcodes.php';
}

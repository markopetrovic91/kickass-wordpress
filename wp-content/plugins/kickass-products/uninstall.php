<?php
/** 
 * Trigger on plugin uninstall
 */

if (!defined('WP_UNINSTALL_PLUGIN')) {
    exit();
}

//  Clear database data
$products = get_posts(array('post_type' => 'products', 'numberposts' => -1));

foreach ($products as $product) {
    wp_delete_post($book->ID, true);
}

// Delete database data via SQL queries

// global $wpdb;
// $wpdb->query("DELETE  FROM wp_posts WHERE post_type = 'products'");
// $wpdb->query("DELETE FROM wp_postmeta WHERE post_id NOT IN (SELECT id FROM wp_posts)");
// $wpdb->query("DELETE FROM wp_term_relationships WHERE object_id NOT IN (SELECT id FROM wp_posts)");

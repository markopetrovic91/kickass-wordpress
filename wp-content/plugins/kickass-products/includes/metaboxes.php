<?php

// Product metaboxes

if (!function_exists('product_link_metabox')) {
    function product_link_metabox()
    {
        add_meta_box('products_metabox_customfields', 'Product Link', 'product_metabox_display', 'products', 'normal', 'high');
    }
}

// Add metabox to the product post type
add_action('add_meta_boxes', 'product_link_metabox');

if (!function_exists('product_metabox_display')) {
    function product_metabox_display()
    {
        global $post;
        $product_link = get_post_meta($post->ID, 'product_link', true);
        ?>
        <style>
            label {
                display: inline-block;
                margin-bottom: 5px;
                margin-left: 5px;
            }

            input#product_link {
                padding: 5px;
                font-size: 15px;
            }
        </style>
        <label for="product_link">Product Link</label><br>
        <div></div>
        <input type="text" name="product_link" id="product_link" placeholder="Product link goes here..." class="widefat" value="<?php echo $product_link; ?>">

    <?php
}
}
function products_posttype_save($post_id)
{
    $is_autosave = wp_is_post_autosave($post_id);
    $is_revision = wp_is_post_revision($post_id);
    // Don't take an action if is autosave or revision
    if ($is_autosave || $is_revision) {
        return;
    }
    $post = get_post($post_id);
    if ($post->post_type == 'products') {
        // save the custom fields data
        if (array_key_exists('product_link', $_POST)) {
            update_post_meta($post_id, 'product_link', sanitize_text_field($_POST['product_link']));
        }
    }
}
add_action('save_post', 'products_posttype_save');

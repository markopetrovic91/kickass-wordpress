<?php

// add shortcode for displaying all products
add_shortcode('all-products', function () {
    $args = array(
        'posts_per_page' => 10,
        'post_type' => 'products'
    );
    $products = get_posts($args);
    ob_start();
    ?>
    <div class="products-wrapper row">
        <?php
        foreach ($products as $product) {
            $post_link = get_post_meta($product->ID, 'product_link', true);
            ?>
            <div class="text-center col-sm-6 col-md-4 product-item post-<?php echo $product->ID; ?>">
                <?php echo get_the_post_thumbnail($product->ID, 'medium'); ?>
                <h3><?php echo $product->post_title; ?></h3>
                <a href="<?php echo $post_link ? $post_link : "#"; ?>" target="_blank" class="more-info">More Info</a>
            </div>
        <?php
    }
    ?>
    </div><!-- .products-wrapper -->
    <?php
    $content = ob_get_contents();
    ob_end_clean();
    return $content;
});

// Product form
add_shortcode('products_form', function () {
    products_save_post_if_submitted();
    $allowed_tags = array('a', 'b', 'strong', 'em');
    ob_start();
    ?>
    <form id="newProductForm" action="" method="POST" enctype="multipart/form-data">
        <?php wp_nonce_field('newpro_form_save', 'newpro_nonce_field'); ?>
        <div class="form-group">
            <label for="productTitle">Title</label>
            <input type="text" class="form-control" id="productTitle" name="productTitle" placeholder="Product title" value="<?php echo (isset($_POST['productTitle']) ? wp_kses($_POST['productTitle'], $allowed_tags) : ''); ?>">
        </div>
        <div class="form-group">
            <label for="productLink">Product link</label>
            <input type="text" class="form-control" id="productLink" name="productLink" placeholder="Product link" value="<?php echo (isset($_POST['productLink']) ? wp_kses($_POST['productLink'], $allowed_tags) : ''); ?>">
        </div>
        <div class="form-group">
            <label for="productImage">Choose an image</label>
            <input type="file" name="productImage" id="productImage"><br>
            <small id="imageHelp" class="form-text text-muted">Formats: jpg | jpeg | png | gif</small>
        </div>

        <button type="submit" class="btn-large more-info mt-20">Submit</button>
    </form>
    <?php
    $form = ob_get_contents();
    ob_end_clean();
    return $form;
});

function products_save_post_if_submitted()
{
    // Stop running function if form is not submitted
    if (!isset($_POST['productTitle']) || !isset($_POST['productLink'])) {
        // echo "<span class=\"error\">Please enter the title and link</span>";
        return;
    }

    // Check the nonce
    if (!wp_verify_nonce($_POST['newpro_nonce_field'], 'newpro_form_save')) {
        echo "<span class=\"error\">The form seems to be invalid. Please try again!</span>";
        return;
    }

    // Fields validation
    if (strlen($_POST['productTitle']) < 3 || trim($_POST['productTitle']) == "") {
        echo "<span class=\"error\">Please enter a title. Titles must be at least three characters long.</span>";
        return;
    }

    if (strlen($_POST['productLink']) < 3 || trim($_POST['productLink']) == "") {
        echo "<span class=\"error\">Please enter a link.</span>";
        return;
    }

    // Check if the link is valid
    if (filter_var($_POST['productLink'], FILTER_VALIDATE_URL) === FALSE) {
        echo "<span class=\"error\">Please enter a valid link.</span>";
        return;
    }

    if ($_FILES['productImage']['error'] == 0) { // if the file is included

        // Check file type
        $allowedTypes = array(IMAGETYPE_PNG, IMAGETYPE_JPEG, IMAGETYPE_GIF);
        $detectedType = exif_imagetype($_FILES['productImage']['tmp_name']);
        $error = !in_array($detectedType, $allowedTypes);

        if ($error) {
            echo "<span class=\"error\">Please choose a valid image format.</span>";
            return;
        }
        $attachment_id = upload_image_file($_FILES['productImage']);
    } else {
        echo "<span class=\"error\">Please choose an image for the product</span>";
        return;
    }

    // Add the content of the form to $post as an array
    $post = array(
        'post_title'    => sanitize_text_field($_POST['productTitle']),
        'post_status'   => 'publish',
        'post_type'     => 'products',
    );
    $postid = wp_insert_post($post);
    if (isset($postid)) {
        update_post_meta($postid, 'product_link', sanitize_text_field($_POST['productLink']));
        if (set_post_thumbnail($postid, $attachment_id)) {
            echo "<span class=\"success\">The product has been successfully saved!</span>";
            $_POST = array();
            wp_redirect(home_url());
        }
    }
}

function upload_image_file($file = array())
{
    require_once(ABSPATH . 'wp-admin/includes/admin.php');
    require_once(ABSPATH . 'wp-admin/includes/image.php');
    require_once(ABSPATH . 'wp-admin/includes/media.php');



    $file_return = wp_handle_upload($file, array('test_form' => false));
    if (isset($file_return['error']) || isset($file_return['upload_error_handler'])) {
        return false;
    } else {
        $filename = $file_return['file'];
        $attachment = array(
            'post_mime_type' => $file_return['type'],
            'post_title' => preg_replace('/\.[^.]+$/', '', basename($filename)),
            'post_content' => '',
            'post_status' => 'inherit',
            'guid' => $file_return['url']
        );

        $attachment_id = wp_insert_attachment($attachment, $file_return['url']);
        $attachment_data = wp_generate_attachment_metadata($attachment_id, $filename);
        wp_update_attachment_metadata($attachment_id, $attachment_data);

        if (0 < intval($attachment_id)) {
            return $attachment_id;
        }
    }
    return false;
}

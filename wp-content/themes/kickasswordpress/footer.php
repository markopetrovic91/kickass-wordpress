<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package kickasswordpress
 */

?>

</div><!-- #content -->

<footer id="footer" class="site-footer text-center">
	<div class="site-info container">
		<div class="copyright text-center">Development &copy; 2019 KickassWP</div>
	</div><!-- .site-info -->
</footer><!-- #footer -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>

</html>
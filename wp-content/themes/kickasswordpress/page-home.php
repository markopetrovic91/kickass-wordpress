<?php
/**
 * Template Name: Home page
 * 
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package kickasswordpress
 */

get_header();
?>

<div id="primary" class="content-area">
    <main id="main" class="site-main">
        <div class="container">
            <?php
            if (have_posts()) :
                /* Start the Loop */
                while (have_posts()) :
                    the_post(); ?>
                    <div class="entry-content">
                        <?php
                        the_content();
                        ?>
                    </div><!-- .entry-content -->
                <?php
            endwhile;
        else :

            get_template_part('template-parts/content', 'none');

        endif;
        ?>
        </div> <!-- #container -->
    </main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();
